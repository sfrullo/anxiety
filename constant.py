VERBOSE = True

STIM_DURATION = 72.0
VIDEO_FRAME_RATE = 61.0

FRAME_RATE_SCALE_FACTOR = 2
FRAME_SIZE_SCALE_FACTOR = 2

TR = 0.05

FACE_MAX_PIXEL = 5
EYE_MAX_PIXEL = 5

EYE_SCALE_FACTOR = 1.3
EYE_MIN_NEIGHBOR = 1

FACE_SCALE_FACTOR = 1.5
FACE_MIN_NEIGHBOR = 2

EYE_TRACKER_SAMPLE_RATE = 60.0

if VERBOSE:
    print 'VERBOSE MODE: ON'
    def message(msg):
        print msg
else:
    def message(msg):
        pass

from common import clock
def profileDecorator(f):
    def wrapper(*arg, **kwarg):
        t = clock()
        res = f(*arg, **kwarg)
        print 'Function <%s> executed in %.2f ms'.format(f.__name__, (clock() - t)*1000 )
        return res
    return wrapper