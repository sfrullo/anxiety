from os import listdir
from os.path import join, isfile, isdir, splitext
from session import Session
from constant import *

class Dataset(object):
    """docstring for Dataset"""
    def __init__(self, dataset_path=None):

        if dataset_path:
            message('Try load dataset from path: {}'.format(dataset_path))
            if isdir(dataset_path) and ('Sessions' in listdir(dataset_path)):
                self.import_directory(dataset_path)
                self.is_file = False
            elif isfile(dataset_path) and splitext(dataset_path)[-1] == 'tsv':
                self.is_file = True
                self.load_tsv_file(dataset_path)
            else:
                raise Exception("{} is neither a valid directory nor a valid .tsv file.".format(dataset_path))

    def import_directory(self, dataset_path):
        path = join(dataset_path, 'Sessions')
        sessions_path = [join(path,s) for s in listdir(path)]
        message('Begin import ..')
        self.sessions = list()
        for s in sessions_path:
            if isfile(join(s,'session.xml')):
                try:
                    message('Try import {}'.format(s))
                    self.sessions.append(Session(xmlpath=join(s,'session.xml')))
                except KeyError as e:
                    message(e.message + '\n')
                except AttributeError as e:
                    message(e.message + '\n')
        message('Total sessions in dataset: {}\n'.format(len(self.sessions)))

    def import_tsv_file(self, file_path):
        self.sessions = list()
        with open(file_path, 'r') as f:
            header = f.readline().replace('\n', '').split('\t')
            print header
            for line in f:
                s = Session()
                data = line.replace('\n', '').split('\t')
                for key,value in zip(header, data):
                    if key in [ 'tau', 'dtau', 'gazeX', 'gazeY' ]:
                        setattr(s, key, map(float, value.split(';')))
                    else:
                        setattr(s, key, value)
                self.sessions.append(s)


    def export_tsv_file(self, file_path, header=None):
        
        if not header:
            header = ['sessionId', 'subjectId', 'feltEmo', 'feltVlnc', 'feltArsl', 'feltCtrl', 'feltPred', \
                      'n_frame_tot', 'facefound', 'eyefound', 'tau', 'dtau', 'tmin', 'tmax', 'td', 'v_ebr', \
                      'gazeX', 'gazeY', 'g_ebr']
        
        with open(file_path, 'w') as f:
            h = '\t'.join(header)
            f.write(h + '\n')
            data_entry = list()
            for s in self.sessions:
                row = str()
                for h in header:
                    data = getattr(s, h)
                    if h in [ 'tau', 'dtau', 'gazeX', 'gazeY' ]:
                        row += ';'.join(str(v) for v in data)
                    else:
                        row += str(data)
                    row += '\t'
                data_entry.append(row)
            f.write('\n'.join(data_entry))
        
        message('Dataset saved at {}'.format(file_path))

    def export_dataset(self, file_path):
        message('Export dataset to {}'.format(file_path))
        import pickle
        with open(file_path, 'wb') as f:
            p = pickle.Pickler(f, pickle.HIGHEST_PROTOCOL)
            for s in self.sessions:
                p.dump(s)

    def import_dataset(self, file_path):
        message('Import dataset from {}'.format(file_path))
        import pickle
        self.sessions = list()
        with open(file_path, 'r') as f:
            p = pickle.Unpickler(f)
            try:
                while True:
                    self.sessions.append(p.load())
            except EOFError:
                message('Import completed')

    def process_all_dataset(self, tr=0.05):
        if self.is_file:
            message('.tsv file was loaded. Nothing to process.')
            return
        else:
            message('Begin dataset processing with tr = {}:'.format(tr))
            for s in self.sessions:
                s.analyze(tr=tr)
            message('End dataset processing with tr = {}:'.format(tr))

        