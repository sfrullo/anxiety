import cv2
import cv2.cv as cv
import numpy as np 

from common import clock, draw_str

from constant import *
from detect_peaks import detect_peaks


class EbrEstimator(object):
    """docstring for BreEstimator"""
    def __init__(self, file=None):
        self.file = file

class GazeEbrEstimator(EbrEstimator):
    """docstring for GazeDataEbrEstimator"""
    def __init__(self, file):
        super(GazeEbrEstimator, self).__init__(file)

        message('Load gaze file.')
        with open(self.file, 'r') as f:
            # skip 24 lines
            for i in range(1,24): f.readline()
            for line in f:
                if line.startswith('Timestamp'):
                    header = line.replace('\n', '').split('\t')
                    data = {h:[] for h in header}
                else:
                    [data[header[i]].append(d) for i,d in enumerate(line.split('\t'))]

        start = [ i for i,v in enumerate(data['Event']) if v == 'MovieStart'][0] + 1
        end = [ i for i,v in enumerate(data['Event']) if v == 'MovieEnd'][0]

        GazePointXLeft = self.parseGazeCoordinate(data['GazePointXLeft'][start:end])
        GazePointYLeft = self.parseGazeCoordinate(data['GazePointYLeft'][start:end])
        GazePointXRight = self.parseGazeCoordinate(data['GazePointXRight'][start:end])
        GazePointYRight = self.parseGazeCoordinate(data['GazePointYRight'][start:end])
        ValidityLeft = self.parseGazeCoordinate(data['ValidityLeft'][start:end])
        ValidityRight = self.parseGazeCoordinate(data['ValidityRight'][start:end])
        
        self.X = self.combineEyes(GazePointXLeft,GazePointXRight,ValidityLeft,ValidityRight,[0,1])
        self.Y = self.combineEyes(GazePointYLeft,GazePointYRight,ValidityLeft,ValidityRight,[0,1])

        self.estimateEbrValue()        

    def parseGazeCoordinate(self, data):
        try: 
            while True:
                data.remove('')
        except ValueError:
            return map(float, data)


    def getGazeData(self):
        return (self.X, self.Y)

    def filtBlink(self, data, msec, sh=False):
        w = int(msec / 1000.0 * EYE_TRACKER_SAMPLE_RATE)
        indx = detect_peaks(data, show=sh)
        for i in indx:
            data[i:i+w] = [False for j in range(w)]
        detect_peaks(data, show=sh)
        return data

    def estimateEbrValue(self):
        
        message('Estimate EBR')
        ebr = list()
        
        # sample per minute
        spm = 60.0 * EYE_TRACKER_SAMPLE_RATE
        
        from math import ceil
        n = ceil(len(self.X) / spm)
        
        len_to_fill = n*spm - len(self.X)
        total_len = int(len(self.X) + len_to_fill)
        
        for i in range(0, total_len, int(spm)):
            message('Minute {}'.format(i/int(spm)))
            start = i
            end = int(i + spm) if i + spm < len(self.X) else len(self.X)

            noeye = map(lambda x: x[0] == -1 == x[1] , zip(self.X[start:end], self.Y[start:end]))
            message('\ttotal_sample = {}'.format(len(noeye)))
            
            # filt blink data to avoid blink faster than 150ms
            noeye = self.filtBlink(noeye,150)

            blink = [True for v1,v2 in zip(noeye, noeye[1:]) if v1 == False and v2 == True]
            message('\ttotal blink = {}'.format(len(blink)))

            message('\tminute relative EBR = {}'.format(len(blink) * spm / len(noeye)))
            ebr.append(len(blink) * spm / len(noeye))

        self.ebr = np.mean(ebr)
        message('mean EBR = {}'.format(self.ebr))


    def combineEyes(self, left_eye, right_eye, left_val, right_val, good_val):
        center = list()
        for l,vl,r,vr in zip(left_eye,left_val, right_eye,right_val):
            if (vl in good_val) and (vr in good_val):
                center.append(np.mean([l,r]))
            elif (vl in good_val) and (vr not in good_val):
                center.append(l)
            elif (vl not in good_val) and (vr in good_val):
                center.append(r)
            else:
                center.append(-1)
        return np.array(center)

    def getEbr(self):
        return self.ebr
        
class VideoEbrEstimator(EbrEstimator):
    """docstring for VideoEbrEstimator"""
    def __init__(self, file_path=None, tr=0, show_frames=False):
        super(VideoEbrEstimator, self).__init__(file_path)
        
        if self.file:
            self.v = cv2.VideoCapture(self.file)

        self.tr = tr
        
        xml_base_path = './xmls/'
        xmls = {'face' : ['haarcascade_frontalface_alt2.xml',
                        'haarcascade_frontalface_alt_tree.xml',
                        'haarcascade_frontalface_alt.xml',
                        'haarcascade_frontalface_default.xml'],
                'eye' : ['haarcascade_lefteye_2splits.xml',
                        'haarcascade_mcs_lefteye.xml',
                        'ojoI.xml']}
        
        face_xmls = [xml_base_path + xml for xml in xmls['face']]
        eye_xmls = [xml_base_path + xml for xml in xmls['eye']]
    
        self.facedetectors = [cv2.CascadeClassifier(xml) for xml in face_xmls]
        self.eyedetectors = [cv2.CascadeClassifier(xml) for xml in eye_xmls]

        self.computeTau(show_frames)
        self.estimateEbrValue()


    def detect(self, img, cascade, sf=2, min_neigh=3):
        rects = cascade.detectMultiScale(img, scaleFactor=sf, minNeighbors=min_neigh, minSize=(30, 30), flags = cv.CV_HAAR_SCALE_IMAGE)
        if len(rects) == 0:
            return np.array([])
        rects[:,2:] += rects[:,:2]
        return rects

    def draw_rects(self, img, rects, color):
        # print 'draw_rects at ', rects 
        for x1, y1, x2, y2 in rects:
            cv2.rectangle(img, (int(x1), int(y1)), (int(x2), int(y2)), color, 2)

    def _computeTau(self, eyeimage, Tr):
        #while cv2.waitKey(1) != ord('q'):
        #    cv2.imshow('v', eyeimage)
        hist, bins = np.histogram(eyeimage, bins=255, range=(0,255), density=True)
        #print hist, bins
        cumhist = hist.cumsum()
        #print cumhist
        tau = max([int(b) for h,b in zip(cumhist,bins) if h <= Tr ])
        return tau

    def computeTau(self, show_frames=False):
        v = self.v

        self.tau = []
        self.face_found = 0
        self.eye_found = 0

        N_FRAME_MAX = STIM_DURATION * VIDEO_FRAME_RATE

        while v.get(cv.CV_CAP_PROP_POS_FRAMES) < N_FRAME_MAX:

            if v.get(cv.CV_CAP_PROP_POS_FRAMES) % FRAME_RATE_SCALE_FACTOR == 0:

                ret, frame = v.read()
                message('Process frame # {}'.format(v.get(cv.CV_CAP_PROP_POS_FRAMES)))

                if ret:
                    if show_frames: vis = frame.copy()
                    
                    img = cv2.cvtColor(frame, cv2.COLOR_RGB2YCR_CB)
                    img = img[:,:,0]
                    
                    # detect faces with all detectors instantiated
                    facebox = [self.detect(img, detector, sf=FACE_SCALE_FACTOR, min_neigh=FACE_MIN_NEIGHBOR) for detector in self.facedetectors]
                    # print 'facebox =', facebox
                    
                    # if at least one detector detects a face
                    if any([fb.any() for fb in facebox]):

                        # select all regions detected only once
                        fbox = [fb for fb in facebox if fb.any()]
                        fbox = np.concatenate(fbox)
                        fbox = np.vstack({tuple(row) for row in fbox})
                        # print 'fbox = ', fbox
                        
                        # compute mean region
                        fbox = np.rint(np.mean(fbox, 0))
                        
                        self.face_found += 1
                        # print 'fbox_mean = ', fbox
                        
                        # draw face rectangle
                        if show_frames: self.draw_rects(vis, [fbox], (255,0,0))
                        
                        # declare list which will contains all eyebox regions detected in all faceboxes detected
                        eyebox = []

                        # THIS IF FOR VERSION WITHOUT MEAN FACE REGION
                        ## for each facebox detected
                        #for fb in fbox:
                        #    draw_rects(vis, fb, (255,0,0))
                            
                        #    # declare ROI in which search for an eye
                        #    for x1,y1,x2,y2 in fb:
                        #        eye_roi = img[y1:(y1+y2)/2, x1:(x1+x2)/2]
                        #        vis_roi = vis[y1:(y1+y2)/2, x1:(x1+x2)/2]
                        #        # add all eyeboxes detected in such a ROI
                        #        eyebox += [(detect(eye_roi, detector), vis_roi) for detector in eyedetectors]
                        
                        for x1,y1,x2,y2 in [fbox]:
                            if show_frames: vis_roi = vis[y1:(y1+y2)/2, x1:(x1+x2)/2]
                            eye_roi = img[y1:(y1+y2)/2, x1:(x1+x2)/2]
                        
                        eyebox += [self.detect(eye_roi, detector, sf=EYE_SCALE_FACTOR, min_neigh=EYE_MIN_NEIGHBOR) for detector in self.eyedetectors]
                        # print 'eyebox = ', eyebox 

                        # if at least one eye region has been detected
                        if any([eb.any() for eb in eyebox]):

                            # select all eye regions detected only once
                            ebox = [eb for eb in eyebox if eb.any()]
                            ebox = np.concatenate(ebox)
                            ebox = np.vstack({tuple(row) for row in ebox})
                            # print 'ebox = ', ebox

                            # compute mean eyregion
                            ebox = np.rint(np.mean(ebox, 0))
                            self.eye_found += 1
                            # print 'ebox_mean = ', ebox

                            ## for each region
                            #for eb,roi in ebox:
                            #    draw_rects(roi, eb, (0,0,255))

                            for x1,y1,x2,y2 in [ebox]:
                                eyeimage = eye_roi[y1:y2, x1:x2]

                            # compute tau value and append it
                            self.tau.append(self._computeTau(eyeimage, self.tr))

                            if show_frames: self.draw_rects(vis_roi, [ebox], (0,0,255))
                        
                        # if no eye region was detected
                        else:
                            if show_frames: draw_str(vis, (20, 60), 'NO EYE at frame: %d ' % (v.get(cv.CV_CAP_PROP_POS_FRAMES)))
                            message('\tNO EYE at frame: %d ' % (v.get(cv.CV_CAP_PROP_POS_FRAMES)))

                            # append last tau value known
                            if len(self.tau) > 0: self.tau.append(self.tau[-1])
                    
                    # if no face region was detected
                    else:
                        if show_frames: draw_str(vis, (20, 40), 'NO FACE at frame: %d ' % (v.get(cv.CV_CAP_PROP_POS_FRAMES)))
                        message('\tNO FACE at frame: %d ' % (v.get(cv.CV_CAP_PROP_POS_FRAMES)))
                        
                        # append last tau value known
                        if len(self.tau) > 0: self.tau.append(self.tau[-1])

                    if show_frames:
                        draw_str(vis, (20, 20), 'frame: {}/{}'.format(int(v.get(cv.CV_CAP_PROP_POS_FRAMES)), int(v.get(cv.CV_CAP_PROP_FRAME_COUNT))))
                        cv2.imshow('image', vis)
            else:
                v.read()
                #message('Skip frame # {}'.format(v.get(cv.CV_CAP_PROP_POS_FRAMES)))

            if show_frames:
                if cv2.waitKey(1) == ord(' '):
                    draw_str(vis, (20, 40), 'PAUSE. Press \'q\' to quit.')
                    cv2.imshow('image', vis)
                    k = cv2.waitKey()
                    if k == ord('q'):
                        cv2.destroyAllWindows()
                        break
                    elif k == ord(' '):
                        continue
                else:
                    continue

        if VERBOSE:
            self.n_frame_tot = int(v.get(cv.CV_CAP_PROP_POS_FRAMES) / FRAME_RATE_SCALE_FACTOR) + 1
            face_err =  self.n_frame_tot - self.face_found
            face_err_rel = 100 * float(face_err) / float(self.n_frame_tot)

            eye_err = self.n_frame_tot - self.eye_found
            eye_err_rel = 100 * float(eye_err) / float(self.n_frame_tot)
            
            print 'Total analyzed frames: ', self.n_frame_tot 
            print 'Total facebox founded: ', self.face_found
            print 'Total eye founded: ', self.eye_found
            print 'Face:\n\terror = {}, \trelative_error = {:.2f} %'.format(face_err, face_err_rel)
            print 'Eye:\n\terror = {}, \trelative_error = {:.2f} %'.format(eye_err, eye_err_rel)

    def getTau(self):
        return self.tau
    
    def getdTau(self):
        return np.diff(self.getTau())

    def getTmin(self):
        return self.tmin

    def getTmax(self):
        return self.tmax

    def getTd(self):
        return self.td

    def getEbr(self):
        return self.ebr

    def estimateEbrValue(self):
        # consider the dtau values
        dtau = self.getdTau()

        if len(dtau) > 0:
            # Compute tmin, tmax and Td
            # we distinguish tiny magnitude from main magnitude as peaks which values
            # are less than the mean between zero and maximum peak value
            indx = detect_peaks(dtau)
            m = np.mean([0, max(dtau[indx])]);
            message('dtau mean value: {}'.format(m))

            # tmax is one unit smaller than the minimum of the main magnitude peaks of dtau
            self.tmax = min(dtau[dtau > m]) - 1;
            message('tmax = {}'.format(self.tmax))
            
            # tmin is chosen in such a way that it must be bigger than the maximum of the tiny magnitude peaks of dtau
            self.tmin = max(dtau[dtau < m]) + 1;
            message('tmin = {}'.format(self.tmin))

            # compute td as td = (tmax - tmin) + 1
            self.td = np.mean([self.tmax,self.tmin]) + 1;
            message('td = {}'.format(m))

            # find indexes in which dtau is greater than td
            indx = detect_peaks(dtau, mph=self.td)

            # for each delta index:
            blink = 0
            for start,stop in zip(indx, indx[1:]):
                # if there is at least an absolute minimum greater than td between each peak > td detected
                if len(detect_peaks( dtau[start:stop], valley=True, mph=self.td)) > 0:
                    # then, it means that there is a blink in this portion of signal, so increment blink
                    blink += 1

            # EBR is the the number of sequential positive and negative peaks greater than td per minute
            minute = STIM_DURATION / VIDEO_FRAME_RATE
            self.ebr = blink / minute
            message('v_ebr = {}\n'.format(self.ebr))
        else:
            raise Exception('Cannot estimate Ebr value: no value in dtau array')

if __name__ == '__main__':
    
    # f = open('dataset_values.tsv', 'r')
    # f.readline()
    # tau = list()
    # for line in f:
    #    t = line.replace('\n', '').split('\t')[-1]
    #    t = map(float, t.split(';'))
    #    tau.append(t)
    
    # for t in tau:
    #    d = VideoEbrEstimator()
    #    d.tau = t
    #    print d.getEbrValue()

    import matplotlib
    matplotlib.use('TkAgg')
    import matplotlib.pylab as plt

    # v_path = '/home/sfrullo/Documents/projects/anxiety/dataset/Sessions/3646/P29-Rec1-2009.09.03.11.51.42_C1 trigger _C_Section_6.avi'
    # v = VideoEbrEstimator(v_path)

    g_path = '/home/sfrullo/Documents/projects/anxiety/dataset/Sessions/1302/P11-Rec1-All-Data-New_Section_2.tsv'
    g = GazeEbrEstimator(g_path)
