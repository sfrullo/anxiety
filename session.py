import numpy as np 
import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt
plt.ion()

from xml.dom import minidom

from constant import * 
from detectors import VideoEbrEstimator, GazeEbrEstimator

class Session(object):
    """docstring for Session"""
    def __init__(self, xmlpath=None):
        
        if xmlpath:
            self.xmlpath = xmlpath
            self.sessionpath = xmlpath.split('session.xml')[0]
            self.sessionId = xmlpath.split('/')[-2]
            # load session info from session.xml file
            self.getSessionInfo(self.xmlpath)
        else:
            self.sessionId = 0 
            self.subjectId = 0     
            self.feltEmo = 0 
            self.feltVlnc = 0 
            self.feltArsl = 0 
            self.feltCtrl = 0 
            self.feltPred = 0

        self.n_frame_tot = 0
        self.facefound = 0
        self.eyefound = 0

        self.tau = [0]
        self.dtau = [0]
        self.tmin = 0
        self.tmax = 0
        self.td = 0
        self.v_ebr = 0
        self.gazeX = [0]
        self.gazeY = [0]
        self.g_ebr = 0

    def analyze(self, video=True, gaze=True, tr=0.05, show_frames=False):
        if video:
            self.tr = tr

            message('[S.{}] Start compute EBR from video data.'.format(self.sessionId)) 
            v_est = VideoEbrEstimator(self.sessionpath + self.video, tr, show_frames)
            message('[S.{}] End compute EBR from video data.'.format(self.sessionId))
            
            self.n_frame_tot = v_est.n_frame_tot
            self.facefound = v_est.face_found
            self.eyefound = v_est.eye_found
            
            self.tau = v_est.getTau()
            self.dtau = v_est.getdTau()
            self.tmin = v_est.getTmin()
            self.tmax = v_est.getTmax()
            self.td = v_est.getTd()
            self.v_ebr = v_est.getEbr()
        
        if gaze:
            message('[S.{}] Start compute EBR from gaze data'.format(self.sessionId))
            g_est = GazeEbrEstimator(self.sessionpath + self.gaze)
            message('[S.{}] End compute EBR from gaze data'.format(self.sessionId))
            self.gazeX, self.gazeY = g_est.getGazeData()
            self.g_ebr = g_est.getEbr()


    def getSessionInfo(self, xmlpath):
        
        try:
            message('Get information for session #{}'.format(self.sessionId))
            
            doc = minidom.parse(xmlpath)

            session = doc.getElementsByTagName('session')
            subject = doc.getElementsByTagName('subject')
            tracks = doc.getElementsByTagName('track')
            annotations = doc.getElementsByTagName('annotation')
            
            fields = ['feltEmo', 'feltArsl', 'feltVlnc', 'feltCtrl', 'feltPred']

            [ setattr(self, f, session.item(0).attributes[f].value) for f in fields ]
            self.subjectId = subject[0].attributes['id'].value
        
            self.video = [ t.attributes['filename'].value for t in tracks if t.hasAttribute('camera') and t.attributes['camera'].value == '1'][0]
            self.gaze = annotations.item(0).attributes['filename'].value
        
        except KeyError as e:
            raise KeyError("Cannot load session # {}: the session has not the attribute {}".format(self.sessionId, e.message))
        
        except IndexError as e:
            raise AttributeError("Cannot load session # {}: some file is not available".format(self.sessionId, e.message))

    def getInfo(self):
        return  [self.sessionId, self.subjectId, self.feltEmo, self.feltVlnc, self.feltArsl, self.feltCtrl, self.feltPred]


    def plot(self, data_name):
        if data_name in ['tau', 'dtau', 'gazeX', 'gazeY']:
            fig_name = 'Plot {} for session {}'.format(data_name, self.sessionId)
            if data_name in ['tau', 'dtau']:
                fig_name += ' with tr = {}'.format(self.tr if hasattr(self, 'tr') else '0.05')
            f = plt.figure(fig_name)
            data = getattr(self, data_name)
            plt.plot(data)
            plt.xlim((0,len(self.dtau)))
            plt.draw()

    def plot_threshold(self):
        fig_name = 'Plot dtau and threshold for session {}'.format(self.sessionId)
        f = plt.figure(fig_name)
        l = list()
        for d,color in zip([self.tmin, self.tmax, self.td],['blue', 'red', 'green']):
            l.append(plt.axhline(xmin=0, xmax=len(self.dtau), y=d, c=color, linewidth=0.5, zorder=0))
        plt.figlegend(l,['tmin', 'tmax', 'td'],'best')
        plt.plot(self.dtau)
        plt.xlim((0,len(self.dtau)))
        plt.draw()
