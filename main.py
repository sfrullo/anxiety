from os import listdir, getcwd
from os.path import join, isfile

import dataset
from constant import *

def analyze_dataset():
    # Analyze the entire dataset loaded in Dataset Class with specified Tr value and without show frames
    d = dataset.Dataset(join(getcwd(), 'dataset'))
    for tr in [0.05]:
        d.process_all_dataset(tr)
        d.export_dataset(join(getcwd(), 'dump/dataset_values_{}_{}_{}_{}.pkl'.format(FRAME_RATE_SCALE_FACTOR, tr, EYE_SCALE_FACTOR, FACE_SCALE_FACTOR)))
        d.export_tsv_file('dump/dataset_values_{}_{}_{}_{}.tsv'.format(FRAME_RATE_SCALE_FACTOR, tr, EYE_SCALE_FACTOR, FACE_SCALE_FACTOR))

def plot_session_dtau():
    # Import a dumped analyzed dataset value and plot tau component and threshold for a given session id
    # is it possible to change data to plot changing the string passed to funcion plot: 
    # possible value are 'tau', 'dtau', 'gazeX', 'gazeY'
    db_path = 'dump/dataset_values_2_0.05_1.3_1.5.pkl'
    d = dataset.Dataset()
    d.import_dataset(db_path)
    s_id = '3512'
    for s in d.sessions:
        if s.sessionId == s_id:
            s.plot('tau')
            s.plot_threshold()

def show_frame_analysis():
    # Show face and eye detection process for each frame of a given session id
    d = dataset.Dataset(join(getcwd(), 'dataset'))
    s_id = '3512'
    for s in d.sessions:
        if s.sessionId == s_id:
            s.analyze(tr = 0.05, show_frames=True)

if __name__ == '__main__':
    
    #####
    # Choose a function to execute
    #####

    #analyze_dataset()
    
    #show_frame_analysis()
    
    plot_session_dtau()